ARG REPO_ARCH

FROM ${REPO_ARCH}/alpine:edge

ENV UID=991
ENV GID=991

RUN \
  mkdir /opt/murmur && \
  addgroup --gid "$GID" murmur && \
  adduser \
  --disabled-password \
  --gecos "" \
  --home /opt/murmur \
  --ingroup murmur \
  --no-create-home \
  --uid "$UID" \
  murmur && \
  chown -R murmur:murmur /opt/murmur && \
  apk add murmur tini

RUN chown murmur:murmur /etc/murmur.ini
RUN ln -s /dev/stdout /var/log/murmur.log

COPY init.sh /usr/local/bin/init.sh

VOLUME /var/lib/murmur

EXPOSE 64738/tcp
EXPOSE 64738/udp

USER murmur
WORKDIR /opt/murmur

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["init.sh", "mumble-server", "-ini", "/etc/murmur.ini", "-fg"]
